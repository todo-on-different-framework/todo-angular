import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './modules/layout/layout.module#LayoutModule' },
  { path: 'auth', loadChildren: './components/auth/auth.module#AuthModule' },
  { path: 'not-found', loadChildren: './components/errors/not-found/not-found.module#NotFoundModule' },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
