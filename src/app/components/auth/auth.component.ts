import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {Form, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {urlValue} from '../../services/url.values';
import {StorageService} from '../../services/storage.service';
import _swal from 'sweetalert2';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  form: FormGroup;

  // Errors
  signInError: boolean = false;
  signInErrorMsg: string = '';
  signUpError: boolean = false;
  signUpErrorMsg: string = '';

  // Change login form from Sign In to Sign Up and opposite
  changeForm: boolean = true;

  constructor(private _api: ApiService,
              private _fb: FormBuilder,
              private _router: Router,
              private _storage: StorageService,
              private _user: UserService) {

    // If user token exist (login state) redirect him to home
    if (this._storage.userToken) {
      this._router.navigate(['/home']);
    }

  }

  ngOnInit() {

    this.form = this._fb.group(
      {
        username : [
          {value: 'ndragun', disabled: !this.changeForm},
          !this.changeForm ? Validators.compose([Validators.email, Validators.required, Validators.minLength(4)]) : null
        ],
        email : ['nemanjadragun@gmail.com', [Validators.email, Validators.required]],
        password : ['12345678',
          [Validators.required, Validators.minLength(4), Validators.maxLength(15), Validators.pattern('[a-zA-Z0-9!?_+-]+')]
        ]
      }
    );

  }

  // return Register Form state and show error based on condition it returns
  get formUsername() { return this.form.get('username'); }
  get formEmail() { return this.form.get('email'); }
  get formPassword() { return this.form.get('password'); }


  onSignIn(value) {
    let body = {
      email: value.email,
      password: value.password
    };
    this._api.send('Post', urlValue.login, body).subscribe(
      (res: any[]) => {

        const username =  res['user'].username;
        this._user.username = username;
        this._storage.set('username', username);

        // USER TOKEN
        const userToken = res['token'];
        this._storage.set('token', userToken);
        this._storage.userToken = userToken;

        // TOKEN DECODE
        const base64Url = userToken.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        const userTokenDecoded = JSON.parse(window.atob(base64));
        this._storage.set('tokenExp', userTokenDecoded['exp']);
        this._storage.userTokenExpTime = userTokenDecoded['exp'];

      },
      (error: Response) => [
        this.signInErrorMsg = error['error'][0].message,
        this.signInError = true
      ],
      () => [
        this._router.navigate(['/home']),
        // Add sweetalert notification on successfull sign in
        _swal({
          position: 'top-end',
          type: 'success',
          title: 'You successfully signed in!',
          showConfirmButton: false,
          timer: 1500,
          customClass: 'custom-top-right-notifications'
        })
      ]
    );
  }

  onSignUp(value) {
    let body = {
      username: value.username,
      email: value.email,
      password: value.password
    };
    this._api.send('Post', urlValue.register, body).subscribe(
      () => {
        this.changeForm = true,
          this.signUpError = false,
          // Add sweetalert notification on successfull sign up
          _swal({
            position: 'top-end',
            type: 'success',
            title: 'You successfully signed up',
            showConfirmButton: false,
            timer: 1500,
            customClass: 'custom-top-right-notifications'
          });
      },
      (error: Response) => [
        this.signUpErrorMsg = error['error'].message,
        this.signUpError = true
      ]
    );
  }

  onChangeForm() {
    this.changeForm = !this.changeForm;
    this.signUpError = false;
  }

  onSubmitForm(value) {
    if (!this.changeForm) this.onSignUp(value);
    else this.onSignIn(value);
  }

}
