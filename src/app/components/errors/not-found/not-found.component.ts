import { Component, OnInit } from '@angular/core';
import {LocationStrategy} from '@angular/common';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(private _location: LocationStrategy) { }

  ngOnInit() {
  }

  goBack() {
    this._location.back();
  }

}
