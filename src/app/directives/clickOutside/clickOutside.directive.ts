import {Directive, Output, EventEmitter, HostListener} from '@angular/core';

@Directive({
  selector: '[appClickOutside]',
})
export class ClickOutsideDirective {
  private localEvent = null;
  @Output('appClickOutside') appClickOutside: EventEmitter<any> = new EventEmitter();


  @HostListener('click') trackEvent(event) {
    this.localEvent = event;
  }

  @HostListener('document: click') compareEvent(event) {
    if (event !== this.localEvent) {
      this.appClickOutside.emit(event);
    }
    this.localEvent = null;
  }

  constructor() {
  }
}
