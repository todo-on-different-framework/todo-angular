import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClickOutsideDirective} from './clickOutside.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
  ],
  declarations: [
    ClickOutsideDirective
  ],
  exports: [
    ClickOutsideDirective
  ]
})
export class ClickOutsideModule {
}
