import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {StorageService} from '../services/storage.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(
    private _router: Router,
    private _storage: StorageService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


    let newReq = request.clone({
      setHeaders: {
        'Authorization': `Bearer ${this._storage.userToken}`,
      }
    });

    return next.handle(newReq).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) return event;
      },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          console.log('error from interceptor');


          // If bad request
          if (err.status === 400)  this._router.navigate(['auth']);

          // If user does not have permission to access the route redirect him to forbidden page
          if (err.status === 403)  this._router.navigate(['not-found']);

          if (err.status === 401) {
            // localStorage.removeItem('profileCompleted');
            // localStorage.removeItem('token');
            // localStorage.removeItem('tokenExp');
            // localStorage.removeItem('userType');
            // localStorage.removeItem('employeeJobType');
            // localStorage.removeItem('isApproved');
            // this._storage.profileCompleted = null;
            // this._storage.userToken = null;
            // this._storage.userTokenExpTime = null;
            // this._storage.userType = null;
            // this._storage.employeeJobType = null;
            // this._storage.isApproved = null;
            // this._router.navigate(['/auth']);

          }

        }
      });
  }
}
