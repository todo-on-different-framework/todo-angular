import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from './layout.component';
import {AuthGuardService} from '../../services/auth-guard.service';

const mainRoutes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      {path: '', redirectTo: 'home'},
      //<editor-fold desc="Home">
      {
        path: 'home',
        loadChildren: './home/home.module#HomeModule'
      },
      //</editor-fold>
      //<editor-fold desc="Todos">
      {
        path: 'todos',
        loadChildren: './todos/todos.module#TodosModule',
      },
      //</editor-fold>
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {
}
