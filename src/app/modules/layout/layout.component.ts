import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {StorageService} from '../../services/storage.service';
import {UserService} from '../../services/user.service';
import {TodosService} from './todos/todos.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewChecked {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;


  constructor(public _storage: StorageService,
              public _user: UserService,
              public _todo: TodosService) {
  }

  ngOnInit() {
    this.scrollToBottom();
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }


}
