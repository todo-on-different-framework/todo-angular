import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutRoutingModule} from './layout-routing.module';
import {LayoutComponent} from './layout.component';
import {UserService} from '../../services/user.service';
import {TodosService} from './todos/todos.service';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule
  ],
  providers: [
    UserService,
    TodosService
  ],
  declarations: [
    LayoutComponent
  ],
  exports: []
})
export class LayoutModule {
}
