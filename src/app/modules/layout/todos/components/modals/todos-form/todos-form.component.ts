import {Component, Input, OnInit} from '@angular/core';
import {TodosService} from '../../../todos.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {urlValue} from '../../../../../../services/url.values';

@Component({
  selector: 'app-todos-form',
  templateUrl: './todos-form.component.html',
  styleUrls: ['./todos-form.component.scss']
})
export class TodosFormComponent implements OnInit {

  createForm: FormGroup;

  @Input() state: boolean = false;

  constructor(public _todosService: TodosService,
              private _formBuilder: FormBuilder) { }

  // return Todos Form state and show error based on condition it returns
  get todoText() { return this.createForm.get('todo_text'); }

  ngOnInit() {
    this.createForm = this._formBuilder.group({
      'todo_text' : ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(200)]
      ],
    });
  }

  onCreateTask(value) {

    this._todosService.addTodo(value);

  }

}
