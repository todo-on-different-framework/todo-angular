import {Component, Input, OnInit} from '@angular/core';
import {TodosService} from '../../../todos.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-todos-modal',
  templateUrl: './todos-modal.component.html',
  styleUrls: ['./todos-modal.component.scss']
})
export class TodosModalComponent implements OnInit {

  updateTaskForm: FormGroup;

  @Input() todoPassedText: string = '';
  @Input() todoPassedId: number = null;
  @Input() passedTodoCreatedAt: any;
  @Input() passedTodoUpdatedAt: any;
  @Input() state: boolean = false;

  constructor(public _todosService: TodosService,
              private _formBuilder: FormBuilder) {
  }

  // return Todos Update Form state and show error based on condition it returns
  get todoText() { return this.updateTaskForm.get('todo_text'); }

  ngOnInit() {

    this.updateTaskForm = this._formBuilder.group({
      'todo_text' : ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(200)]
      ],
    });
  }

  onUpdateTodo(param) {

    this._todosService.updateTodo(param.todo_text, this.todoPassedId);

  }

}
