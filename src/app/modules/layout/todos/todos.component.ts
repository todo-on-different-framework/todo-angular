import { Component, OnInit } from '@angular/core';
import {TodosService} from './todos.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  passedTodoText: string = '';
  passedTodoId: number = null;

  passedTodoCreatedAt: any;
  passedTodoUpdatedAt: any;

  updateForm: FormGroup;

  constructor(public _todosService: TodosService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {

    this.updateForm = this._formBuilder.group({
      'todo_text' : ['']
    });
  }

  passTodoText(param, todoId, createdAt, updatedAt) {
    this.passedTodoText = param;
    this.passedTodoId = todoId;
    this.passedTodoCreatedAt = createdAt;
    this.passedTodoUpdatedAt = updatedAt;

  }


}
