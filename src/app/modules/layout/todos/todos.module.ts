import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TodosRoutingModule} from './todos-routing.module';
import {TodosComponent} from './todos.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TodosModalComponent } from './components/modals/todos-modal/todos-modal.component';
import {TodosService} from './todos.service';
import {ClickOutsideModule} from '../../../directives/clickOutside/clickOutside.module';
import { TodosFormComponent } from './components/modals/todos-form/todos-form.component';
import {MomentModule} from 'ngx-moment';

@NgModule({
  imports: [
    CommonModule,
    TodosRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    // https://github.com/urish/ngx-moment
    MomentModule
  ],
  providers: [
  ],
  declarations: [
    TodosComponent,
    TodosModalComponent,
    TodosFormComponent
  ],
  exports: [
  ]
})
export class TodosModule {
}
