import {Injectable} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {urlValue} from '../../../services/url.values';
import _swal from 'sweetalert2';

@Injectable()
export class TodosService {

  public showTodoForm: boolean = false;
  private completeTodoStatus: boolean = false;
  public totalTodosGet: number = 0;

  public showTodoDetails: boolean = false;
  public todo_items = [];
  public showError: boolean = false;
  public errorSameTaskSingle: number = null;
  public showEditItem: number = null;

  constructor(private _api: ApiService) {
    this.getTodo();
  }

  getTodo() {
    this._api.send('Get', urlValue.getTodo).subscribe((res: any[]) => {
      this.todo_items = res['data'];
      this.getTotalTodos();
    });
  }

  getTotalTodos() {
    this._api.send('Get', urlValue.getTotalTodos).subscribe((res: any[]) => {
      this.totalTodosGet = res['data'][0].totalTodos;
    });
  }

  addTodo(param) {
    this._api.send('Post', urlValue.createTodo, {'todo_text': param.todo_text}).subscribe((res) => {
      this.getTodo();
      _swal({
        title: 'Do you want to add more tasks after this one?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#e10873',
        cancelButtonColor: '#8b8b8b',
        confirmButtonText: 'Yes, i want to add more!',
        customClass: 'custom-top-right-notifications'
      }).then((result) => {
        _swal({
          position: 'top-end',
          type: 'success',
          title: `New todo ${param.todo_text} has been added!`,
          showConfirmButton: false,
          timer: 3000,
          customClass: 'custom-top-right-notifications'
        });
        if (!result.value) {
          this.showTodoForm = false;
        }
      });

    });
  }

  deleteTodo(param, value) {
    this._api.send('Delete', urlValue.deleteTodo + param).subscribe((res) => {
      this.getTodo();
      _swal({
        position: 'top-end',
        type: 'success',
        title: `Todo ${value} has been deleted!`,
        showConfirmButton: false,
        timer: 3000,
        customClass: 'custom-top-right-notifications'
      });
    });
  }

  deleteAllTodos() {
    _swal({
      title: 'Are you sure that you want to delete your whole todo list?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#e10873',
      cancelButtonColor: '#8b8b8b',
      confirmButtonText: 'Yes, delete it!',
      customClass: 'custom-top-right-notifications'
    }).then((result) => {
      if (result.value) {
        this._api.send('Delete', urlValue.deleteAllTodos).subscribe(res => {
          this.getTodo();
        });
        _swal({
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          customClass: 'custom-top-right-notifications'
          }
        );
      }
    });

  }

  updateTodo(param, taskId) {
    this._api.send('Patch', urlValue.updateTodo + taskId, {'todo_text': param}).subscribe(() => {
      this.getTodo();
      this.showTodoDetails = false;
      _swal({
        position: 'top-end',
        type: 'success',
        title: `Todo number ${taskId} has been updated with new text ${param}!`,
        showConfirmButton: false,
        timer: 3000,
        customClass: 'custom-top-right-notifications'
      });
    });
  }

  completeTodo(param, status, value) {
    if (status === 1) this.completeTodoStatus = false;
    else this.completeTodoStatus = true;

    this._api.send('Patch', urlValue.updateTodo + param, {'completed': this.completeTodoStatus}).subscribe(() => {
      this.getTodo();
      _swal({
        position: 'top-end',
        type: 'success',
        title: `Todo ${value} has been ${this.completeTodoStatus ? 'completed' : 'not-completed'}!`,
        showConfirmButton: false,
        timer: 3000,
        customClass: 'custom-top-right-notifications'
      });
    });
  }

}
