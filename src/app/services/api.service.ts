import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ApiService {

  constructor(private _http: HttpClient) {
  }

  // Methods: Get, Post, Put, Patch, Delete
  send = (method: string, url: string, body?: any) => {

    let options = {
      body: body,
    };

    return this._http.request(method, url, options);

  };
}
