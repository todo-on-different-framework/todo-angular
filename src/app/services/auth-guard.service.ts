import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {StorageService} from './storage.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  currTime: any;
  expTime: any;

  constructor(private _route: Router,
              private _storage: StorageService) {}

  canActivate() {
      if (this._storage.userToken) {
        // check if token is valid
        this.currTime = Math.round(+new Date() / 1000);
        this.expTime = this._storage.userTokenExpTime;
        if (this.currTime < this.expTime) {
          return true;
        } else {
          this._storage.onLogout();
          this._route.navigate(['/home']);
        }
      } else {
        // IF NOT LOGGED IN OR TOKEN EXPIRED GO BACK TO LOGIN
        this._route.navigate(['/auth']);
        return false;
      }
  }
}
