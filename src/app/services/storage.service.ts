import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import _swal from 'sweetalert2';

@Injectable()
export class StorageService {
  public userToken: string;
  public userTokenExpTime: string;

  constructor(private _router: Router) {
    this.userToken = this.get('token');
    this.userTokenExpTime = this.get('tokenExp');
  }

  set(name: string, value?: any): void {
    if (!value) localStorage.removeItem(name);
    else {
      if (typeof value !== 'string') value = JSON.stringify(value);
      localStorage.setItem(name, value);
    }
  }

  get(name: string): any {
    let item = localStorage.getItem(name);
    try {
      item = JSON.parse(item);
    } catch (err) {
    }
    return item;
  }

  update(name: string, added: any): void {
    let current: any = localStorage.getItem(name);
    try {
      current = JSON.parse(current);
    } catch (err) {
    }

    if (!current) current = {};

    for (let key in added) current[key] = added[key];

    localStorage.setItem(name, JSON.stringify(current));
  }

  delete(name: string): void {
    localStorage.removeItem(name);
  }

  clear(): void {
    localStorage.clear();
  }

  onLogout() {
    this.clear();
    this.userToken = null;
    this.userTokenExpTime = null;
    this._router.navigate(['/auth']);
    // Add sweetalert notification on successfull log out
    _swal({
      position: 'top-end',
      title: 'You just logged out. See you next time!',
      showConfirmButton: false,
      timer: 1500,
      customClass: 'custom-top-right-notifications'
    });
  }
}
