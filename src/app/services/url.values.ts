const baseUrl = 'http://localhost:3333/api/';
const apiVersion = 'v1';

export const urlValue = {
  base: baseUrl,

  register: `${baseUrl + apiVersion}/register`,
  login: `${baseUrl + apiVersion}/login`,

  getTodo: `${baseUrl + apiVersion}/todo/get`,
  getTotalTodos: `${baseUrl + apiVersion}/todo/get/total`,
  createTodo: `${baseUrl + apiVersion}/todo/create`,
  deleteTodo: `${baseUrl + apiVersion}/todo/delete/`,
  deleteAllTodos: `${baseUrl + apiVersion}/todo/deleteAll`,
  updateTodo: `${baseUrl + apiVersion}/todo/update/`,

};
