import { Injectable } from '@angular/core';
import {StorageService} from './storage.service';

@Injectable()
export class UserService {

  public username: string = '';

  constructor(private _storage: StorageService) {
    this.username = this._storage.get('username');
  }

}
